## Calculate the volatility, volume, avg volume, avg bid, avg ask, and avg spread
# of all csv files of stock prices in a certain folder
# Finished 6/14/2018
# simpleCols2.py

import pandas as pd
import numpy as np
import os
from datetime import datetime
from multiprocessing import Pool, cpu_count

# User can change these vars
root_dir = '' # relative path
dirs = ['dailyTick']
dir_paths = [os.path.join(root_dir, i) for i in dirs]

# Make sure the specified folders exist
for path in dir_paths:
	if not os.path.isdir(path):
		print('Missing folder:', path)
		dir_paths.remove(path)
print('Folders found:', dir_paths)
if len(dir_paths) == 0:
	print('No valid folders found. Terminating.')
	quit()

stat_names = ['Volatility', 'Volume', 'AvgVolume', 'AvgBid1', 'AvgAsk1', 'AvgBidVolume1', 'AvgAskVolume1', 'AvgSpread']
intervals = {'5m': 5*60*1000, '30m': 30*60*1000, '1d': 60*60*24*1000} # {key: value} where value is in seconds

start_time = datetime.now()

def _insertCol(df, name):
	df.insert(loc = df.shape[1], column = name, value = [np.nan] * df.shape[0])

def getStartIndexes(df):
	''' start after the X mins of stock quotes, otherwise stats could be misleading '''
	start_indexes = {interval : df.shape[0] - 1 for interval in intervals}
	for interval in intervals:
		last_negative = 0 # when lastPrice == -1
		for find_first_row_iter in range(df.shape[0]):
			if df.loc[find_first_row_iter, 'LastPrice'] < 0:
				last_negative += 1
				# print('last_negative += 1 \t #=> last_negative == {0}'.format(last_negative))
				continue
			if df.loc[find_first_row_iter, 'TimeStamp'] - df.loc[last_negative]['TimeStamp'] >= intervals.get(interval):
				start_indexes[interval] = find_first_row_iter
				print ('start_index = {0} for {1}'.format(start_indexes.get(interval), interval))
				break
			if find_first_row_iter == df.shape[0] - 1:
				print ('start_index = {0} for {1}'.format(start_indexes.get(interval), interval))
	return start_indexes

def addSpread(df):
	''' Calculate the spread '''
	_insertCol(df, 'Spread')
	df.loc[df['LastPrice'] >= 0, 'Spread'] = df.loc[:, 'AskPrice1'] - df.loc[:, 'BidPrice1']

def addMidPrice():
	''' Calculate midPrice '''
	_insertCol(df, 'MidPrice')
	df.loc[:, 'MidPrice'] = (1/2) * (df.loc[:, 'AskPrice1'] + df.loc[:, 'BidPrice1'])

def addStatCols(df, start_indexes, file):
	''' Generate a new column for each stat for each time frame '''
	for name in stat_names:
		for interval in intervals:
			addOneStatCol(df, start_indexes, name, interval, file) if _generateColName(name, interval) not in df.columns else None

def addOneStatCol(df, start_indexes, name, interval, file):
	''' Add just one column '''
	col_name = _generateColName(name, interval)
	_insertCol(df, col_name)
	for curr_row in range(df.shape[0]):
		doSpecificOperation(df, start_indexes, name, interval, curr_row, col_name, file)

def _generateColName(name, interval):
	col_name = name + '_' + interval
	return col_name

def doSpecificOperation(df, start_indexes, name, interval, curr_row, col_name, file):
	if (curr_row < start_indexes.get(interval)):
		return
	min_timestamp = df.loc[curr_row, 'TimeStamp'] - intervals.get(interval)
	if name == 'Volatility':
		high =  df.loc[(df['TimeStamp'] >= min_timestamp) & (df['TimeStamp'] <= df.loc[curr_row, 'TimeStamp']) & (df['LastPrice'] >= 0), 'HighPrice'].max()
		low = df.loc[(df['TimeStamp'] >= min_timestamp) & (df['TimeStamp'] <= df.loc[curr_row, 'TimeStamp']) & (df['LastPrice'] >= 0), 'LowPrice'].min() 
		lastPrice = df.loc[curr_row, 'LastPrice']
		df.loc[curr_row, col_name] = (high - low) / lastPrice
	elif name == 'Volume':
		high = df.loc[(df['TimeStamp'] >= min_timestamp) & (df['TimeStamp'] <= df.loc[curr_row, 'TimeStamp']) & (df['LastPrice'] >= 0), 'Volume'].max()
		low = df.loc[(df['TimeStamp'] >= min_timestamp) & (df['TimeStamp'] <= df.loc[curr_row, 'TimeStamp']) & (df['LastPrice'] >= 0), 'Volume'].min()
		if high == low:
			df.loc[curr_row, col_name] = df.loc[curr_row, 'Volume'] - df.loc[curr_row - 1, 'Volume']
		else:
			df.loc[curr_row, col_name] = high - low
	elif name == 'AvgVolume':
		cells = df.loc[(df['TimeStamp'] >= min_timestamp) & (df['TimeStamp'] <= df.loc[curr_row, 'TimeStamp']) & (df['LastPrice'] >= 0), 'Volume']
		high = cells.max()
		low = cells.min()
		if high == low:
			df.loc[curr_row, col_name] = df.loc[curr_row, 'Volume'] - df.loc[curr_row - 1, 'Volume']
		else:
			df.loc[curr_row, col_name] = (high - low) / cells.count()
	elif name == 'AvgBid1':
		df.loc[curr_row, col_name] = df.loc[(df['TimeStamp'] >= min_timestamp) & (df['TimeStamp'] <= df.loc[curr_row, 'TimeStamp']) & (df['LastPrice'] >= 0), 'BidPrice1'].mean()
	elif name == 'AvgAsk1':
		df.loc[curr_row, col_name] = df.loc[(df['TimeStamp'] >= min_timestamp) & (df['TimeStamp'] <= df.loc[curr_row, 'TimeStamp']) & (df['LastPrice'] >= 0), 'AskPrice1'].mean()
	elif name == 'AvgBidVolume1':
		df.loc[curr_row, col_name] = df.loc[(df['TimeStamp'] >= min_timestamp) & (df['TimeStamp'] <= df.loc[curr_row, 'TimeStamp']) & (df['LastPrice'] >= 0), 'BidVolume1'].mean()
	elif name == 'AvgAskVolume1':
		df.loc[curr_row, col_name] = df.loc[(df['TimeStamp'] >= min_timestamp) & (df['TimeStamp'] <= df.loc[curr_row, 'TimeStamp']) & (df['LastPrice'] >= 0), 'AskVolume1'].mean()
	elif name == 'AvgSpread':
		df.loc[curr_row, col_name] = df.loc[(df['TimeStamp'] >= min_timestamp) & (df['TimeStamp'] <= df.loc[curr_row, 'TimeStamp']) & (df['LastPrice'] >= 0), 'Spread'].mean()
	print('Elapsed time: {0}\t{1}:\t[{2}, {3}] = {4}'.format(
			datetime.now() - start_time,
			file, 
			curr_row, col_name, 
			df.loc[curr_row, col_name]))

def writeCSV(df, file):
	''' Write finished CSV to disk '''
	df.to_csv(file, index = False) # don't include the row indexes as an additional column
	print('Successfully wrote {0}'.format(file))

def listCSV(path):
	names = os.listdir(path)
	name_paths = [os.path.join(path, name) for name in names]
	csv_paths = [path for path in name_paths if os.path.isfile(path) and path.endswith('.csv')]
	return csv_paths

def listDirs(path):
	names = os.listdir(path)
	name_paths = [os.path.join(path, name) for name in names]
	dir_paths = [path for path in name_paths if os.path.isdir(path)]
	return dir_paths

# def parallelize_dataframe(df, func, partitions=10, cores=cpu_count()):
# 	# df_split = np.array_split(df, partitions, axis=1)
# 	# with Pool(cores) as pool:
# 	# 	df = pd.concat(pool.map(func, df_split))
# 	# return df
# 	df_split = np.array_split(df, partitions, axis=1)
# 	with Pool(cores) as pool:
# 			df = pd.concat(pool.map(func, partition))
# 	return df

def evalFile(file):
	print('\n====================\n')
	print('On file {0}...'.format(file))

	df = pd.read_csv(file)

	start_indexes = getStartIndexes(df)
	addSpread(df) if 'Spread' not in df.columns else None

	#df = parallelize_dataframe(df, addStatCols(df, start_indexes, file))
	# addSpread(df) if 'Spread' not in df.columns else ''
	# addMidPrice(df)s
	addStatCols(df, start_indexes, file)
	writeCSV(df, file)

for path in dir_paths:
	for folder in listDirs(path):
		for file in listCSV(folder):
			evalFile(file)
